const jwt = require( 'koa-jwt' );
var jwtSign = require( 'jsonwebtoken' );
const secret = process.env.JWT_SECRET || 'e9492ef1-69e3-4831-b13f-83c665d813d5';
const mongo = require( 'koa-mongo' );
const sendmail = require( 'sendmail' )();


module.exports.login = function ( ctx , next ) {
  if ( ctx.request.body.password === '654321' ) {
    ctx.status = 200;
    ctx.body = {
      token: jwtSign.sign( { emailVerified: false , userId: '5da9e5a1e50e781e8c81f3b0' } , secret ),
      message: "Авторизация успешна!"
    };
  } else {
    ctx.status = ctx.status = 401;
    ctx.body = {
      errors: {
        main: "Ошибка аутентификации"
      }
    };
  }
  return ctx;
}

module.exports.register = async function ( ctx , next ) {
  ctx.checkBody( 'email' ).notEmpty( 'Введите почту' ).isEmail( "Неправильный формат почты." );
  ctx.checkBody( 'password' ).notEmpty( 'Введите пароль' ).len( 3 , 20 , 'Длина пароля должна быть между 3 и 20' );
  ctx.checkBody( 'password_confirm' ).notEmpty( 'Введите подтверждение пароля' ).eq( ctx.request.body.password , 'Пароли не совпадают' );
  ctx.checkBody( 'friend_code' ).optional().len( 4 , 7 , 'Неправильный код друга' ).toInt( 'Неправильный код друга' );
  if ( ctx.errors ) {
    ctx.status = 401;
    ctx.body = {
      errors: ctx.errors
    };
  } else {
    const check = await ctx.db.collection( 'users' ).find({ email:ctx.request.body.email }).count();
    if ( check == 0 ) {
      const result = await ctx.db.collection( 'users' ).insert( ctx.request.body );
      const userId = result.ops[ 0 ]._id.toString()
      // ctx.body = await ctx.db.collection( 'users' ).find().toArray();
      ctx.status = 200;
      ctx.body = {
        token: jwtSign.sign( { userId } , secret ),
        message: "Авторизация успешна!"
      };
    } else {
      ctx.status = ctx.status = 401;
      ctx.body = {
        errors: {
          email: "Пользователь с такой почтой существует"
        }
      };
    }
  }
  return ctx;
}



module.exports.verifyEmail = async function ( ctx , next ) {
  if (!ctx.headers.authorization) ctx.throw(403, 'No token.');
  const token = ctx.headers.authorization.split( ' ' )[ 1 ];
  try {
    ctx.request.jwtPayload = jwtSign.verify( token , secret );
    const userId = ctx.request.jwtPayload.userId;
    let user = await ctx.db.collection( 'users' ).findOne({_id:mongo.ObjectId( userId )});
    let email = user.email;
    let code = Math.floor( 1000 + Math.random() * 9000 );
    await ctx.db.collection( 'users' ).updateOne(
      { _id: mongo.ObjectId( userId ) },
      { $set: { "code" : code } },
      { upsert: false } 
     );
    sendmail({
      from: 'no-reply@dev.action.am',
      to: email ,
      subject: 'test sendmail',
      html: `Ваш код регистрации - ${code}` ,
    }, function( err , reply ) {
      console.log(err && err.stack);
    });
  } catch ( err ) {
    // console.log(err)
    ctx.throw(err.status || 403, err.text);
  }

  await next();
}

module.exports.verifyEmailCode = async function ( ctx , next ) {
  ctx.checkBody( 'code' ).notEmpty( 'Введите код' ).len( 4 , 4 , 'Длина кода должна быть 4' );
  console.log(ctx.errors)
  if ( ctx.errors ) {
    ctx.status = 401;
    ctx.body = {
      errors: ctx.errors
    };
  } else {
    if (!ctx.headers.authorization) ctx.throw(403, 'No token.');
    const token = ctx.headers.authorization.split( ' ' )[ 1 ];
    try {
      ctx.request.jwtPayload = jwtSign.verify( token , secret );
      const userId = ctx.request.jwtPayload.userId;
      let user = await ctx.db.collection( 'users' ).findOne({_id:mongo.ObjectId( userId )});
      let email = user.email;
      let code = user.code;
      if ( ctx.request.body.code == code ) {
        await ctx.db.collection( 'users' ).updateOne(
          { _id: mongo.ObjectId( userId ) },
          { $set: { "emailVerified" : 1 } },
          { upsert: false } 
        );
        ctx.status = 200;
      } else {
        ctx.status = 401;
        ctx.body = {
          errors: { code: 'Неправильный код регистрации' }
        };
      }
    } catch ( err ) {
      console.log(err)
      ctx.throw( err.status || 403 , err.text );
    }
  }
  return ctx;
}
