'use strict';

module.exports.test = async (ctx,next) => {
	// console.log(ctx)
  ctx.body = {ha:'ha'};
  await next();
};

module.exports.getUsers = async (ctx,next) => {
	// console.log(ctx)
	ctx.body = await ctx.db.collection( 'users' ).find().toArray();
  	await next();
};


