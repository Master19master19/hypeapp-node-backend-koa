const jwt = require( 'jsonwebtoken' );
const secret = process.env.JWT_SECRET || 'e9492ef1-69e3-4831-b13f-83c665d813d5';




module.exports.auth = async ( ctx , next ) => {
	if (!ctx.headers.authorization) ctx.throw(403, 'No token.');
	const token = ctx.headers.authorization.split(' ')[1];
	try {
		ctx.request.jwtPayload = jwt.verify( token , secret );
		if ( undefined == ctx.request.jwtPayload.emailVerified ) {
			ctx.throw( 302 );
		}
	} catch ( err ) {
		ctx.throw(err.status || 403, err.text);
	}

  	await next();
};