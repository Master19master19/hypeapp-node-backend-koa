'use strict';
const Route = require( 'koa-router' );
const router = new Route();
const Koa = require( 'koa' );
const app = new Koa();
const port = process.env.PORT || 3000;
const session = require( 'koa-session' );
const bodyParser = require( 'koa-bodyparser' );
const compress = require( 'koa-compress' );
const logger = require( 'koa-logger' );
require( 'koa-validate' )( app );
const koaBody = require( 'koa-body' );
app.use( koaBody({ multipart:true , formidable:{keepExtensions:true}}) );
const mongo = require( 'koa-mongo' );


app.use(mongo({
  uri: 'mongodb://localhost:27017/hypeapp',
  max: 100,
  min: 1
}));


const authRoutes = require( './app/controller/auth' );
const testController = require( './app/controller/test' );
const jwtMiddleware = require('./app/middleware/jwt');



router.use( [ '/test' ] , jwtMiddleware.auth );


router.post( '/login', authRoutes.login );
router.post( '/register', authRoutes.register );
router.get( '/verifyEmail' , authRoutes.verifyEmail );
router.post( '/verifyEmailCode' , authRoutes.verifyEmailCode );
router.get( '/getUsers' , testController.getUsers );
router.post( '/test', testController.test )










app.use( session( {} , app ) );
app.use( bodyParser() );
app
  .use(router.routes())
  .use(router.allowedMethods());
app.use( logger() );
app.use( compress() );
app.listen( port , () => console.log( `Listening on port ${port}` ) );

